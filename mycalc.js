
(function($){
	Drupal.behaviors.mycalc = {
		attach:function (context) {
		 
			$('.calc_number', context).keyup(function() {
				var settings = {url:'/ajax/mycalc', progress: false, 
					submit: { n1: $('#first_number').val(), n2: $('#second_number').val() }
				};    
				var ajax = new Drupal.ajax(false, false, settings);
				ajax.eventResponse(ajax, {});
			});
		}
	}
})(jQuery)